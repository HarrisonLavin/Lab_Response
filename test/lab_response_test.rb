require "test_helper"
require 'minitest/autorun'
require_relative "../lib/lab_response/constants"
require_relative '../lib/lab_response.rb'

class LabResponseTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::LabResponse::VERSION
  end


  describe "full lifecycle tests" do

    before do 
      @survey = LabResponse::Survey.new(LabResponseConstants::ABOUT_PAGE, 3, 10, 6)
      
      # while I understand that monkey-patching is frowned upon, having the pause in is painful
      def monkey_patch
        def @survey.pause_for_next_request
          unless @responses.length >= @requests
            puts LabResponseConstants::PAUSE
          end
        end
      end
    end


    def test_success_code_received
      Net::HTTP.stub :get_response, Net::HTTPResponse.new(2, LabResponseConstants::SUCCESS_CODE, "bar") do
        # uncomment the following line if you want to skip the ten second wait between requests
        # monkey_patch
        result = @survey.full_communication_cycle 
        assert_equal LabResponseConstants::SERVICE_UP, result[:status]
      end
    end

    def test_failure_code_received
      Net::HTTP.stub :get_response, Net::HTTPResponse.new(2, LabResponseConstants::FAILURE_CODE, "bar") do
        # uncomment the following line if you want to skip the ten second wait between requests
        # monkey_patch
        result = @survey.full_communication_cycle 
        assert_equal LabResponseConstants::SERVICE_DOWN, result[:status]
      end
    end

  end

  describe 'initializing survey through CLI' do

    def test_standard_behavior
      result = %x(lab_response)
      assert_equal "tolerating 3 failures", result.scan(/tolerating \d+ failures/).first
      assert_equal "at 10 second intervals", result.scan(/at \d+ second intervals/).first
      assert_equal "Making 6 requests", result.scan(/Making \d+ requests/).first
      assert_equal "to #{LabResponseConstants::ABOUT_PAGE},", result.scan(/to \S+,/).first
    end

    def test_flag_changes
      result = %x(lab_response -t 3 -i 1 -r 2 -p home)
      assert_equal "tolerating 3 failures", result.scan(/tolerating \d+ failures/).first
      assert_equal "at 1 second intervals", result.scan(/at \d+ second intervals/).first
      assert_equal "Making 2 requests", result.scan(/Making \d+ requests/).first
      assert_equal "to #{LabResponseConstants::HOME_PAGE},", result.scan(/to \S+,/).first
    end

    def test_zeros
      result = %x(lab_response -t 0 -i 0)
      assert_equal "tolerating 0 failures", result.scan(/tolerating \d+ failures/).first
      assert_equal "at 0 second intervals", result.scan(/at \d+ second intervals/).first
    end
  end

  describe 'test error handling' do
    # All of the below tests test that incorrect input into the CLI raises an exception
    # Naturally, this will raise errors in the output, but the tests and functionality work!
    def test_incorrect_input_for_tolerate
      refute system("lab_response -t foo")
      refute system("lab_response --tolerate foo")
    end

    def test_incorect_input_for_interval
      refute system("lab_response -i bar")
      refute system("lab_response --interval bar")
    end

    def test_incorrect_input_for_requests
      refute system("lab_response -r foo")
      refute system("lab_response --requests foo")
      refute system("lab_response --requests 0")
      refute system("lab_response -r 0")
    end

    def test_incorect_input_for_page
      refute system("lab_response -p foo")
      refute system("lab_response --page bar")
    end

  end

  describe 'unit test methods' do
    before do
      @survey = LabResponse::Survey.new(LabResponseConstants::ABOUT_PAGE, 3, 1, 6)
      5.times{|v| @survey.responses.push(LabResponseConstants::SUCCESS_CODE)}
      5.times{|v| @survey.aggregate_response_time += 0.14}
    end

    def test_compile
      @survey.responses.push(LabResponseConstants::SUCCESS_CODE)
      @survey.aggregate_response_time += 0.14
      result = @survey.compile
      assert_equal 0.14, result[:avg_response_time]
      assert_equal LabResponseConstants::SERVICE_UP, result[:status]
    end

    def test_ping
      Net::HTTP.stub :get_response, Net::HTTPResponse.new(2, LabResponseConstants::FAILURE_CODE, "bar") do
        time_before = @survey.aggregate_response_time
        @survey.ping
        time_after = @survey.aggregate_response_time
        assert_equal [LabResponseConstants::SUCCESS_CODE,
                      LabResponseConstants::SUCCESS_CODE,
                      LabResponseConstants::SUCCESS_CODE,
                      LabResponseConstants::SUCCESS_CODE,
                      LabResponseConstants::SUCCESS_CODE,
                      LabResponseConstants::FAILURE_CODE], @survey.responses
        assert time_before != time_after
      end
    end

    def test_pause_for_next_request_if_more_requests_to_be_done
      time_before = Time.now  
      @survey.pause_for_next_request
      time_after = Time.now
      assert time_before != time_after
    end

    def test_pause_for_next_request_if_requests_completed
      @survey.responses.push(LabResponseConstants::SUCCESS_CODE)
      mocked_method = MiniTest::Mock.new
      mocked_method.expect :sleep, false, [1]
      refute @survey.pause_for_next_request
    end

    def test_make_request
      assert_output(/Response received./){ @survey.make_request}
      assert_output(/Beginning request.../){@survey.make_request}
    end


  end
  
end