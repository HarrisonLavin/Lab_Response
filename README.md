# LabResponse
This gem provides a CLI to check the status of https://gitlab.com or https://about.gitlab.com and reports an average response time after probing the site every 10 seconds for  one minute. 

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'lab_response'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install lab_response

## Usage

To run lab_response, simply enter `lab_response` into your terminal after installing.  Optionally, you may customize the parameters for its Survey using the flags `--page` (`-p`), `--tolerate` (`-t`), `--interval` (`-i`), and/or `--requests` (`-r`).
