require "lab_response/version"
require_relative "lab_response/constants"
require 'net/http'
require 'uri'

module LabResponse
  class Survey
    attr_reader :page, :failures_to_tolerate, :interval_to_pause, :requests
    attr_accessor :aggregate_response_time, :responses

    def initialize(page, failures, inter, req)
      @page = page || LabResponseConstants::ABOUT_PAGE
      @failures_to_tolerate = failures || 3
      @interval_to_pause = inter || 10
      @requests = req || 6
      @aggregate_response_time = 0.00
      @responses = []
    end

    def full_communication_cycle
      puts "Making #{@requests} requests to #{@page}, " + 
        "at #{@interval_to_pause} second intervals, tolerating #{@failures_to_tolerate} failures"
      @requests.times do
        make_request
      end
      compile
    end

    def make_request
      puts LabResponseConstants::START_PING
      ping
      puts LabResponseConstants::GOT_RESPONSE
      pause_for_next_request
    end

    def pause_for_next_request
      unless @responses.length >= @requests
        puts LabResponseConstants::PAUSE
        sleep(@interval_to_pause)
      end
    end

    def ping
      uri = URI.parse(@page)
      time_request_sent = Time.now
      response = Net::HTTP.get_response(uri)    
      time_response_received = Time.now
      @aggregate_response_time += (time_response_received - time_request_sent)
      @responses << response.code
    end

    def compile
      puts LabResponseConstants::DONE
      number_of_failures = @responses.find_all{|code| LabResponseConstants::FAILURE_REGEX.match(code)}.size
      status = number_of_failures >= @failures_to_tolerate ? LabResponseConstants::SERVICE_DOWN : LabResponseConstants::SERVICE_UP
      avg_response_hash= {avg_response_time: (@aggregate_response_time / @requests),
                          status: status}
    end
  end

end
