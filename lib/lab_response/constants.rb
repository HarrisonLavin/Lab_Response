module LabResponseConstants
    SUCCESS_CODE = "200".freeze
    FAILURE_CODE = "500".freeze
    FAILURE_REGEX= /5\d{2}/.freeze
    ABOUT_PAGE = 'https://about.gitlab.com'.freeze
    HOME_PAGE = 'https://gitlab.com'.freeze
    START_PING = "Beginning request...".freeze
    GOT_RESPONSE = "Response received.".freeze
    PAUSE= "Waiting to make another request...".freeze
    DONE = "Requests Complete!".freeze
    SERVICE_UP = "Service does not exceed fault tolerance limits, is likely up".freeze
    SERVICE_DOWN = "Service exceeds fault tolerance limits, is likely down".freeze
end
